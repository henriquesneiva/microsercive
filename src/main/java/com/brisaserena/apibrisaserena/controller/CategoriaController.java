package com.brisaserena.apibrisaserena.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.brisaserena.apibrisaserena.domain.Categoria;
import com.brisaserena.apibrisaserena.repositoy.CategoriaRepository;

@RestController
@RequestMapping("/api/categorias")
public class CategoriaController {

	@Autowired
	private CategoriaRepository repository;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Categoria Creat(@RequestBody Categoria categoria) {
		return repository.save(categoria);
	}
	
	@GetMapping
	public List<Categoria> findAll(){
		return repository.findAll();
	}
	
	@GetMapping("{id}")
	public Categoria findById(@PathVariable Integer id) {
		return repository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,"categoria não encontrada"));
	}
	
	@PutMapping("{id}")
	public void update(@PathVariable Integer id, @RequestBody Categoria categriaUpdate) {
		repository.findById(id)
		.map(categoria -> {
			categriaUpdate.setId(categoria.getId());
			return repository.save(categriaUpdate);
		}).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,"categroia não encontrada"));
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable Integer id ) {
		repository.findById(id)
			.map(categroia -> {
				repository.delete(categroia);
				return Void.TYPE;
			}).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Categroia não encontrada"));
	}
	
}
