package com.brisaserena.apibrisaserena.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.brisaserena.apibrisaserena.domain.Produto;
import com.brisaserena.apibrisaserena.repositoy.ProdutoRepository;

@RestController
@RequestMapping("/api/produtos")
public class ProdutoController {

	@Autowired
	private ProdutoRepository repository;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Produto crate(@Valid @RequestBody Produto produto) {
		return repository.save(produto);
	}
	
	@GetMapping
	public List<Produto> findAll(){
		return repository.findAll();
	}
	
	@GetMapping("{id}")
	public Produto findByIdo(@PathVariable Integer id) {
		return repository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,"produto não encontrado"));
	}
	
	@PutMapping("{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public  void update(@PathVariable Integer id, @RequestBody Produto produtoUpadate) {
		
		repository.findById(id)
		.map(produto -> {
			produtoUpadate.setId(produto.getId());
			return repository.save(produtoUpadate);
		}).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Produto não encontrado"));
		}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable Integer id) {
		
		repository.findById(id)
		.map(produto -> {
			repository.delete(produto);
			return Void.TYPE;
		}).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Produto não encontrado"));
		
	}
	
	
}
