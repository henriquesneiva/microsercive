package com.brisaserena.apibrisaserena.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Produto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotNull(message = "o campo nome e´obrigatorio")
	private String nome;
	@NotNull(message = "o campo descricao e´obrigatorio")
	private String descricao;
	@NotNull(message = "o campo preço e´obrigatorio")
	private double preco;
	@NotNull(message = "o campo quantidade e´obrigatorio")
	private int quantidade;

}
