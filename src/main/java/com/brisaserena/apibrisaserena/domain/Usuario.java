package com.brisaserena.apibrisaserena.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotNull(message = "o campo nome e´obrigatorio")
	private String nome;
	@CPF
	@NotNull(message = "o campo cpf e´obrigatorio")
	private String cpf;
	@Email
	@NotNull(message = "o campo e-mail e´obrigatorio")
	private String email;
	@NotNull(message = "o campo senha e´obrigatorio")
	private String senha;
	
}
