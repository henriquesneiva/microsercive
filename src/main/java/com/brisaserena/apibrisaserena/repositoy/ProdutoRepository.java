package com.brisaserena.apibrisaserena.repositoy;

import org.springframework.data.jpa.repository.JpaRepository;

import com.brisaserena.apibrisaserena.domain.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Integer>{

}
