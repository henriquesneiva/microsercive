package com.brisaserena.apibrisaserena;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrisaserenaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrisaserenaApiApplication.class, args);
	}

}
